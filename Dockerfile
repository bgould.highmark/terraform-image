FROM gcr.io/google.com/cloudsdktool/cloud-sdk

RUN (echo "Installing terraform" && apt-get -qq update && apt-get -qq install unzip && cd /tmp && curl -Lo terraform.zip https://releases.hashicorp.com/terraform/1.0.3/terraform_1.0.3_linux_amd64.zip && unzip terraform.zip && mv $(pwd)/terraform /usr/local/bin/terraform)
COPY HighmarkHealthRootCA.crt /usr/local/share/ca-certificates
RUN (echo "Trusting Highmark root CA" && update-ca-certificates)